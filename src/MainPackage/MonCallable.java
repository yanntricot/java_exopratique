/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MainPackage;

import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author trico
 */
public class MonCallable implements Callable<Integer>{

    private int compteur;
    private int min;
    private int max;
    
    public MonCallable(int Min, int Max){
        this.min = Min;
        this.max = Max;
    }
    
    @Override
    public Integer call(){
        
        for(int i = min; i<max; i++){
            compteur+=i;
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                Logger.getLogger(MonCallable.class.getName()).log(Level.SEVERE, null, ex);
            }
        }return compteur;         
    }
}